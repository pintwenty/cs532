#include <stdio.h>
void insertion_sort(int n, int arr[]);

int main()
{
    int i,q;
    printf("Enter number of elements you want to sort :\n");
    scanf("%d",&q);
    int arr[q];
    for(i=0;i<q;i++)
    {
        scanf("%d",&arr[i]); 
    }

    insertion_sort(q, arr); 

    //print the sorted array
    printf("Sorted array\n");
    for(i=0;i<q;i++)
    {
        printf("%d ",arr[i]);
    }
    printf("\n");
    return 0;
}

void insertion_sort(int n, int arr[])
{
    int i,j,k;
    for (i = 1; i < n; i++) {
        k = arr[i];
        j = i - 1;
        while (j >= 0 && arr[j] > k) {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = k;
    }
}

/*
1. run gcc insertionsort.c to compile
2. run ./a.out to execute the code
*/