#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
#include <sys/stat.h>
#include<unistd.h>

const char *filename = "./readdir_modified.c";

int main(int argc, char *argv[]) {
    struct stat sb;

    if (stat(filename, &sb) == -1) {
        perror("stat");
        exit(EXIT_FAILURE);
    }

    printf("Total file size: %lld bytes\n", sb.st_size);
    exit(EXIT_SUCCESS);
}