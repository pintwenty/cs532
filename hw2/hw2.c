/*
Name: Santosh Pasnoor
BlazeID: santosh1
Project #: Howmework 2
To compile: Run gcc readdir_modified.c to compile
To run: Run ./a.out <directory>/<substring>/<size> to execute the code
*/
#include <stdio.h> 
#include <stdlib.h>
#include <dirent.h> 
#include<string.h>
#include <fcntl.h>
#include <time.h>
#include <sys/stat.h>
#include<unistd.h>
void listFilesRecursively(char *current);
void filesWithSize(char *current,int x);
void filesWithSubString(char *current,char *sub);
void filesOnly(char *current);
void directoriesOnly(char *current);
void sizeWithSubString(char *current,int x, char* sub);
typedef struct { char *key; int val; } t_symstruct;
#define NKEYS (sizeof(lookuptable)/sizeof(t_symstruct)) //lookup table for remapping flags to int
int atoi(const char *string); //string to integer type
static t_symstruct lookuptable[] = {
    { "-S", 1 }, { "-s", 2 }, { "-f", 3 }, { "-t", 4 }};

int keyfromstring(char *key) // created similar to getopt function 
{
    int i;
    for (i=0; i < NKEYS;i++) {
        t_symstruct *sym = &lookuptable[i];
        if (strcmp(sym->key, key) == 0)
            return sym->val;
    }
    return -1;
}

char *filetype(unsigned char type) { //defines all posssible file types
  char *str;
  switch(type) {
  case DT_BLK: str = "block device"; break;
  case DT_CHR: str = "character device"; break;
  case DT_DIR: str = "directory"; break;
  case DT_FIFO: str = "named pipe (FIFO)"; break;
  case DT_LNK: str = "symbolic link"; break;
  case DT_REG: str = "regular file"; break;
  case DT_SOCK: str = "UNIX domain socket"; break;
  case DT_UNKNOWN: str = "unknown file type"; break;
  default: str = "UNKNOWN";
  }
  return str;
}

void Commands(char* argv[],int argc) //command function which takes flags as inputs
{
  int x =0;
  switch(keyfromstring(argv[1]))
  {
    
    case 1:
      printf("All Files and Its Directories with Size\n");
      if (argc<3)
      {
        listFilesRecursively(".");
      }
      else{
        listFilesRecursively(argv[2]);
      }
      break;
    case 2:
      x = atoi(argv[2]);
      printf("Files greater than %d bytes\n",x);
      filesWithSize(argv[3],x);
      break;
    case 3:
      printf("Files with Sub String\n");
      filesWithSubString(argv[3],argv[2]);
      break;
    case 4:
      if(strcmp(argv[2],"f")==0)
      {
        printf("Files Only\n");
        filesOnly(".");
        break;
      }
      else if(strcmp(argv[2],"d") == 0)
      {
      printf("Directories Only\n");
      directoriesOnly(".");
      break;
      }
    default:
      printf("Syntax Error\n");
      break;

  }
}
int getFileSize(char *c) //function to get the size of a file
{
   char *filename = c;
            struct stat sb;
            if (stat(filename, &sb) == -1) {
                perror("stat");
                exit(EXIT_FAILURE);
            }
            return(sb.st_size);
}
void listFilesRecursively(char *current) //displays all files in current directory and its sub directories.
{
    char path[1000];
    struct dirent *dirent;
    DIR *dir = opendir(current);
    if (!dir)
        return;
    while ((dirent = readdir(dir)) != NULL)
    {
        if (strcmp(dirent->d_name, ".") != 0 && strcmp(dirent->d_name, "..") != 0)
        {
          char *c = malloc(sizeof(current) + sizeof("/")+sizeof(dirent->d_name)); //create dynamic temporary character pointer
          strcpy(c, current);
          strcat(c,"/");
          strcat(c,dirent->d_name);
          printf("%s (%d)\n",dirent->d_name,getFileSize(c)); 
            // Construct new path from our base path
            strcpy(path, current);
            strcat(path, "/");
            strcat(path, dirent->d_name);
            // recursice call
            listFilesRecursively(path);
        }
    }
    closedir(dir);
}


void filesWithSize(char *current,int x) //outputs files which are greater than given size
  {
    char path[1000];
    struct dirent *dirent;
    DIR *dir = opendir(current);
    if (!dir)
        return;
    while ((dirent = readdir(dir)) != NULL)
    {
        if (strcmp(dirent->d_name, ".") != 0 && strcmp(dirent->d_name, "..") != 0)
        {
          char *c = malloc(sizeof(current) + sizeof("/")+sizeof(dirent->d_name));
          strcpy(c, current);
          strcat(c,"/");
          strcat(c,dirent->d_name);
            if(getFileSize(c) >= x)//calling getfile function
            {
               printf("%s (%d)\n",dirent->d_name,getFileSize(c));
            }            
            // Construct new path from our base path
            strcpy(path, current);
            strcat(path, "/");
            strcat(path, dirent->d_name);
            // recursice call
            filesWithSize(path,x);
        }
    }
    closedir(dir);

  }
//function prints all files with size greter than given size and 
// starts with specified substring
void sizeWithSubString(char *current,int x, char* sub) 
{
    char path[1000];
    struct dirent *dirent;
    DIR *dir = opendir(current);
    if (!dir)
        return;
    while ((dirent = readdir(dir)) != NULL)
    {
        if (strcmp(dirent->d_name, ".") != 0 && strcmp(dirent->d_name, "..") != 0)
        {
          char *c = malloc(sizeof(current) + sizeof("/")+sizeof(dirent->d_name));
          strcpy(c, current);
          strcat(c,"/");
          strcat(c,dirent->d_name);
          if(getFileSize(c) >= x)
          {
            if(strstr(dirent->d_name,sub))
            {
              printf("%s (%d)\n",dirent->d_name,getFileSize(c));
            }
              
          }           
            // Construct new path from our base path
            strcpy(path, current);
            strcat(path, "/");
            strcat(path, dirent->d_name);
            // recursice call
            sizeWithSubString(path,x,sub);
        }
    }
    closedir(dir);
}

//this functions returns all the files with 
//the substring specified
void filesWithSubString(char *current, char *sub)
  {
    char path[1000];
    struct dirent *dirent;
    DIR *dir = opendir(current);
    if (!dir)
        return;
    while ((dirent = readdir(dir)) != NULL)
    {
        if (strcmp(dirent->d_name, ".") != 0 && strcmp(dirent->d_name, "..") != 0)
        {
          if(strstr(dirent->d_name,sub))
          {
            printf("%s\n",dirent->d_name); 
          } 
          strcpy(path, current);
          strcat(path, "/");
          strcat(path, dirent->d_name);
            
          filesWithSubString(path,sub);
        }
    }
    closedir(dir);

  }

//this function returns only files present in the current directory or speciffied path
void filesOnly(char *current)
{
    char path[1000];
    struct dirent *dirent;
    DIR *dir = opendir(current);
    if (!dir)
        return;
    while ((dirent = readdir(dir)) != NULL)
    {
        if (strcmp(dirent->d_name, ".") != 0 && strcmp(dirent->d_name, "..") != 0)
        {
          if(dirent->d_type == DT_REG)
          {
            printf("%s\n",dirent->d_name);
          } 
            strcpy(path, current);
            strcat(path, "/");
            strcat(path, dirent->d_name);
            filesOnly(path);
        }
    }
    closedir(dir);
}

//this function returns only directories of specified path
void directoriesOnly(char *current)
{
char path[1000];
    struct dirent *dirent;
    DIR *dir = opendir(current);
    if (!dir)
        return;
    while ((dirent = readdir(dir)) != NULL)
    {
        if (strcmp(dirent->d_name, ".") != 0 && strcmp(dirent->d_name, "..") != 0)
        {
          if(dirent->d_type == DT_DIR)
          {
            printf("%s\n",dirent->d_name);
          } 
            strcpy(path, current);
            strcat(path, "/");
            strcat(path, dirent->d_name);
            directoriesOnly(path);
        }
    }
    closedir(dir);
}


int main(int argc,char* argv[])
{
    DIR *parentDir;
  if (argc < 2) { 
    printf ("Usage: %s <dirname>\n", argv[0]); 
    exit(-1);
  } 
  Commands(argv,argc); //commands function which uses switch case to call appropriate function calls
}
