
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

int main()
{
    int pid = fork();
    if (pid == 0)
    {
        pid = getpid();
        char str[10];
        sprintf(str, "%d", pid);
        strcat(str, ".txt");
        printf("%s\n", str);
    }
    return 0;
}