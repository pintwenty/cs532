**Simple Word Counter Program**

we use data structures, dynamic memory allocation and simple string fucntions.

**Description:**

**Creating a dynamic table (array of structs) based on the command** **line  arguments**

**Reading text from standard input**

**Tokenizing the text and creating a dynamic array**

**Comparing keywords and updating keyword count**

**Display the keyword/count table**

**Use of suitable functions to perform the different tasks above**

**Instructions to run the code:**

1. run gcc hw1.c to compile
2. run ./a.out <'text file' `<arguments>` to execute the code
