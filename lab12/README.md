**Lab 12**


implementing thread and synchronization using mutexes



 **Simple Pthread Program to find the sum of a vector.**

 **Uses mutex locks to update the global sum.**

 **Author: Santosh Pasnoor**

 **Date: Apr 8, 2022**

 **To Compile: gcc -O test -Wall test.c**

 **To Run: ./test 1000 4**
