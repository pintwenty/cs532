/*
  Simple Pthread Program to find the sum of a vector.
  Uses mutex locks to update the global sum.
  Author: Santosh Pasnoor
  Date: Apr 8, 2022

  To Compile: gcc -O test -Wall test.c 
  To Run: ./test 1000 4
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

// double *a=NULL, sum=0.0;
// int    N, size;

struct foo
{
  double *a;
  double sum;
  int N;
  int size;
  long tid;
};

void *compute(void *arg)
{
  struct foo *info = (struct foo *)arg;
  int myStart, myEnd, myN, i;
  long tid = info->tid;

  // determine start and end of computation for the current thread
  myN = info->N / info->size;
  myStart = tid * myN;
  myEnd = myStart + myN;
  if (tid == (info->size - 1))
    myEnd = info->N;

  // compute partial sum
  double mysum = 0.0;
  for (i = myStart; i < myEnd; i++)
    mysum += info->a[i];

  // grab the lock, update global sum, and release lock
  pthread_mutex_lock(&mutex);
  info->sum += mysum;
 pthread_mutex_unlock(&mutex);

  return (NULL);
}

int main(int argc, char **argv)
{
  int i;
  int N, size;
   pthread_t *tid;
  
  double *a=NULL;
  if (argc != 3)
  {
    printf("Usage: %s <# of elements> <# of threads>\n", argv[0]);
    exit(-1);
  }

  N = atoi(argv[1]);    // no. of elements
  size = atoi(argv[2]); // no. of threads
  a = (double *)malloc(sizeof(double)*N);

  // allocate structure
  struct foo *info = malloc(sizeof(struct foo) * size);
   
  // allocate vector and initialize
   tid = (pthread_t *)malloc(sizeof(pthread_t)*size);
  // info->a = (double *)malloc(sizeof(double) * N);
  for (i = 0; i < N; i++)
    a[i] = (double)(i + 1);
    
  // create threads
  for (i = 0; i < size; i++)
  {
    info[i].tid = i;
    info[i].N = N;
    info[i].size = size;
    info[i].a = a;
    pthread_create(&tid[i], NULL, compute, (void *)&info[i]);
  }
   
  // wait for them to complete
  for (i = 0; i < size; i++)
    pthread_join(tid[i], NULL);
    
  double sum=0;
  for(int i=0;i<size;i++)
  sum = sum+ info[i].sum;
  printf("The total is %g, it should be equal to %g\n",
         sum, ((double)N * (N + 1)) / 2);
  free(info);
  return 0;
}