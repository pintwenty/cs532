/*
  Simple Pthread Program to find the sum of a vector.
  Uses mutex locks to update the global sum.
  Author: Santosh Pasnoor
  Date: Apr 8, 2022

  To Compile: gcc -O p -Wall pthread_psum.c -lpthread
  To Run: ./p 1000 4
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

// double *a=NULL, sum=0.0;
// int    N, size;

typedef struct foo
{
  pthread_t ptid;
  int tid;
  double a, sum;
  int N, size;
} FOO;

void *compute(void *arg)
{
  FOO *info = (FOO *)arg;
  int myStart, myEnd, myN, i;
  long tid = (long)arg;

  // determine start and end of computation for the current thread
  myN = info->N / info->size;
  myStart = tid * myN;
  myEnd = myStart + myN;
  if (tid == (info->size - 1))
    myEnd = info->N;

  // compute partial sum
  double mysum = 0.0;
  for (i = myStart; i < myEnd; i++)
    mysum += info[i].a;

  // grab the lock, update global sum, and release lock
  pthread_mutex_lock(&mutex);
  info->sum += mysum;
  pthread_mutex_unlock(&mutex);

  return (NULL);
}

int main(int argc, char **argv)
{
  FOO *info;
  long i;
  int size, N;
  // pthread_t *tid;

  if (argc != 3)
  {
    printf("Usage: %s <# of elements> <# of threads>\n", argv[0]);
    exit(-1);
  }

  N = atoi(argv[1]);    // no. of elements
  size = atoi(argv[2]); // no. of threads

  // allocate structure
  info = (FOO *)malloc(sizeof(FOO)* size);

  // allocate vector and initialize
  // tid = (pthread_t *)malloc(sizeof(pthread_t) * size);
  // a = (double *)malloc(sizeof(double) * N);
  for (i = 0; i < N; i++)
    info[i].a = (double)(i + 1);

  // create threads
  for (i = 0; i < size; i++)
  {
    info[i].N = N;
    info[i].size = size;
    pthread_create(&info[i].ptid, NULL, compute, (void *)&info[i]);
  }

  // wait for them to complete
  for (i = 0; i < size; i++)
    pthread_join(info[i].ptid, NULL);

  printf("The total is %g, it should be equal to %g\n",
         info->sum, ((double)N * (N + 1)) / 2);

  free(info);
  return 0;
}
