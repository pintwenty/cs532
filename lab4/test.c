#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<fcntl.h>
#include<string.h>

#define BUFFSIZE 4096

int main(int argc, char *argv[])
{
    int readFile, writeFile;
    long int n;
    char buf[BUFFSIZE];
    if(argc!=3)
    {
        puts("enter 2 file names");
    }
    readFile = open(argv[1],O_RDONLY,S_IRWXG);
    writeFile = open(argv[2],O_TRUNC|O_CREAT|O_WRONLY,S_IRWXG);
    if(readFile == -1 || writeFile == -1)
    {
        puts("cannot open file");
    }
    //  read(readFile,buf,BUFFSIZE);
    //  write(writeFile,buf,BUFFSIZE);
    while((n = read(readFile,buf,BUFFSIZE)) > 0)
    {
        if(n<0)
        {
            puts("error reading file");
        }
        // printf("%ld\n",n);
        else if(write(writeFile,buf,n) != n)
        {
            puts("cannot write file");
        }
        else
        {
            puts("file copied successfully");
        }
    }
    close(readFile);
    close(writeFile);
    return 0;
}