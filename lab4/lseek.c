#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<fcntl.h>

#define BUFFSIZE 4096

int main(int argc, char *argv[])
{
    int readFile, writeFile;
    long int n;
    char buf[BUFFSIZE];
    if(argc!=3)
    {
        puts("enter 2 file names");
        exit(-1);
    }
    readFile = open(argv[2],O_RDONLY,S_IRWXG);
    writeFile = open(argv[1],O_WRONLY,S_IRWXG);
    if(readFile == -1 || writeFile == -1)
    {
        puts("cannot open file");
        exit(-1);
    }

    while((n = read(readFile,buf,BUFFSIZE)) > 0)
    {
        lseek(writeFile,0,SEEK_END);
        if(n<0)
        {
            puts("error reading file");
            exit(-1);
        }
        // printf("%ld\n",n);
        if(write(writeFile,buf,n) != n)
        {
            puts("cannot write file");
            exit(-1);
        }
        else
        {
            puts("content copied successfully");
        }
    }

    close(readFile);
    close(writeFile);
    return 0;
}

/*
1. run gcc insertionsort.c to compile
2. run ./a.out to execute the code
*/