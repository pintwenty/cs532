**Lab 09**


/* Simple program to illustrate the use of fork-exec-wait pattern using signal handling.

* This version uses execvp and command-line arguments to create a new process.
* To Compile: gcc -Wall forkexecvp.c
* To Run: ./a.out `<command>` [args]

 */
