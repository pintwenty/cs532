/* Simple program to illustrate the use of fork-exec-wait pattern using signal handling.
 * This version uses execvp and command-line arguments to create a new process.
 * To Compile: gcc -Wall forkexecvp.c
 * To Run: ./a.out <command> [args]
 */

/*
Name: Santhosh Pasnoor
BlazerId: santosh1
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

int pid;
int parent_pid;

void sig_usr(int signo)
{
    signal(signo, SIG_IGN);         /* ignore other SIGINT signals */
    switch (signo)
    {
    case SIGINT:
        printf("Child Process %d Interrupted. Parent : %d\n", pid, getpid());
        kill(pid, SIGINT);
        break;
    case SIGTSTP:
        printf("Child Process %d Suspended. Parent : %d\n", pid, getpid());
        kill(pid, SIGTSTP);
        break;
    case SIGQUIT:
        printf("Exiting Parent process : %d\n", getpid());
        // raise(SIGQUIT);
        exit(-1);
    }
}

int main(int argc, char **argv)
{

    int status;
    if (argc < 2)
    {
        printf("Usage: %s <command> [args]\n", argv[0]);
        exit(-1);
    }

    pid = fork();
    parent_pid = getpid();
    if (pid == 0)
    { /* this is child process */
        // pause();
        execvp(argv[1], &argv[1]);
        printf("If you see this statement then execl failed ;-(\n");
        exit(-1);
    }
    else if (pid > 0)
    { /* this is the parent process */
        printf("Waiting for child process %d to terminate.\n", pid);
        if (signal(SIGINT, sig_usr) == SIG_ERR)
        {
            printf("can't catch SIGINT\n");
            exit(-1);
        }
        if (signal(SIGTSTP, sig_usr) == SIG_ERR)
        {
            printf("can't catch SIGINT\n");
            exit(-1);
        }
        if (signal(SIGQUIT, sig_usr) == SIG_ERR)
        {
            printf("can't catch SIGINT\n");
            exit(-1);
        }
        wait(&status); /* wait for the child process to terminate */
        if (WIFEXITED(status))
        { /* child process terminated normally */
            printf("Child process exited with status = %d\n", WEXITSTATUS(status));
        }
        else
        { /* child process did not terminate normally */
            printf("Child process did not terminate normally! Now in Parent process : %d\n", getpid());
            if (status == 2)
            {
                for (;;)
                {
                    pause();
                }
            }

            /* look at the man page for wait (man 2 wait) to determine
               how the child process was terminated */
        }
    }
    else
    {                   /* we have an error */
        perror("fork"); /* use perror to print the system error message */
        exit(EXIT_FAILURE);
    }

    printf("[%ld]: Exiting program .....\n", (long)getpid());

    return 0;
}
