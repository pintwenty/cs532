#include <stdio.h> 
#include <stdlib.h>
#include <dirent.h> 
#include<string.h>
void listFilesRecursively(char *current);


int main(int argc,char* argv[])
{
    DIR *parentDir;
  if (argc < 2) { 
    printf ("Usage: %s <dirname>\n", argv[0]); 
    exit(-1);
  } 
  parentDir = opendir (argv[1]); 
  if (parentDir == NULL) { 
    printf ("Error opening directory '%s'\n", argv[1]); 
    exit (-1);
  }
  listFilesRecursively(argv[1]);

}

void listFilesRecursively(char *current)
{
    char path[1000];
    struct dirent *dirent;
    DIR *dir = opendir(current);
    if (!dir)
        return;

    while ((dirent = readdir(dir)) != NULL)
    {
        if (strcmp(dirent->d_name, ".") != 0 && strcmp(dirent->d_name, "..") != 0)
        {
            printf("%s\n", dirent->d_name);
            // Construct new path from our base path
            strcpy(path, current);
            strcat(path, "/");
            strcat(path, dirent->d_name);
            // recursice call
            listFilesRecursively(path);
        }
    }
    closedir(dir);
}

/*
1. run gcc readdir_modified.c to compile
2. run ./a.out <directory> to execute the code
*/